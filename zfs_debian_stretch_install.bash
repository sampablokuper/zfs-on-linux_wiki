#!/usr/bin/env bash
###
# See copyright notice at end of file.
###
# Intended usage
# ==============
# This script will install Debian Stretch onto a drive (HDD, SSD, SSHD, or
# flash drive) as described in
# Debian-Stretch-Boot-on-ZFS-Root-On-LUKS-Plus-ZFS.org
###

# Has set-up script been sourced? If not, source it.
[[ -v COMMON_SOURCED && "$COMMON_SOURCED" -eq 1 ]] \
	|| source zfs_debian_stretch_common.bash $1
wait

# Create ZFS datasets for the root ("/") and /boot filesystems
zfs create -o canmount=noauto -o mountpoint=/      "$ROOT_POOL"/debian
zfs create -o canmount=noauto -o mountpoint=/boot  "$BOOT_POOL"/debian
 
# Mount the root ("/") and /boot ZFS datasets
# N.B. With ZFS, it is not normally necessary to use a mount command
# (either `mount` or `zfs mount`). This situation is an exception because of
# `canmount=noauto` having been used above.
zfs mount "$ROOT_POOL"/debian
zfs mount "$BOOT_POOL"/debian

# Create datasets for subdirectories
# The primary goal of this dataset layout is to separate the OS from user data.
# This allows the root filesystem to be rolled back without rolling back user
# data such as logs (in `/var/log`). This will be especially important if/when
# a `beadm` or similar utility is integrated. Since we are creating multiple
# datasets anyway, it is trivial to add some restrictions (for extra security)
# at the same time. The `com.sun.auto-snapshot` setting is used by some ZFS
# snapshot utilities to exclude transient data.
zfs create                     -o setuid=off              "$ROOT_POOL"/home
zfs create -o mountpoint=/root                            "$ROOT_POOL"/home/root
zfs create -o canmount=off     -o setuid=off  -o exec=off "$ROOT_POOL"/var
zfs create -o com.sun:auto-snapshot=false                 "$ROOT_POOL"/var/cache
zfs create                                                "$ROOT_POOL"/var/log
zfs create                                                "$ROOT_POOL"/var/mail
zfs create                                                "$ROOT_POOL"/var/spool
zfs create -o com.sun:auto-snapshot=false     -o exec=on  "$ROOT_POOL"/var/tmp
zfs create                                                "$ROOT_POOL"/srv
zfs create -o com.sun:auto-snapshot=false     -o exec=on  "$ROOT_POOL"/tmp

# Set the `bootfs` property. ***TODO: IS THIS CORRECT???***
zpool set bootfs="$ROOT_POOL"/debian "$ROOT_POOL"
 
# Set correct permission for tmp directories
chmod 1777 /mnt/tmp
chmod 1777 /mnt/var/tmp

# Copy Debian install from source drive to target drive
for i in "${DIRS_TO_COPY[@]}"; do 
    rsync --archive --quiet --delete /"$i"/ /mnt/"$i"
done
for i in "${FILES_TO_COPY[@]}"; do
    cp -a /"$i" /mnt/
done
for i in "${VIRTUAL_FILESYSTEM_DIRS[@]}"; do
    # Make mountpoints for virtual filesystems on target drive
    mkdir /mnt/"$i"
    # Recursively bind the virtual filesystems from source environment to the
    # target. N.B. This is using `--rbind`, not `--bind`.
    mount --rbind /"$i"  /mnt/"$i"
    # Protect from chroot per https://unix.stackexchange.com/q/362870
    mount --make-rslave /mnt/"$i"
done

# Copy, to target drive, a script to configure GRUB2 and /boot . This script
# should be run from within a chroot. Also copy common script.
cp -a zfs_debian_stretch_chroot.bash \
      zfs_debian_stretch_common.bash \
      /mnt/

# Ensure those scripts are owned by root
chown root:root /mnt/zfs_debian_stretch_*.bash

# Ensure those scripts have rwx permissions for root & none for anyone else
chmod 700 /mnt/zfs_debian_stretch_*.bash

# `chroot` into the target environment
chroot /mnt /bin/bash --login

# ***TODO*** For security, disable the *creation* of "device nodes" on the
# drives (i.e.  things like /dev/urandom or /dev/sdX . This should be the FINAL
# STEP of the install process, lest it scupper creation of legitimate device
# nodes.
# ARCH WIKI SEEMS TO THINK ONLY /tmp NEEDS PROTECTION LIKE THIS???
# zfs set devices=off "$ROOT_POOL"
# zfs set devices=off "$BOOT_POOL"
# ***TODO***
# ARCH WIKI ALSO RECOMMENDS THIS FOR SECURITY:
# zfs set setuid=off <pool>/tmp
# ARCHI WIKI ALSO RECOMMENDS:
# systemctl mask tmp.mount



# Step 6: First Boot
# ------------------
# 
# 6.1  Snapshot the initial installation:
# 
#     # zfs snapshot "$ROOT_POOL"/debian@install
# 
# In the future, you will likely want to take snapshots before each upgrade,
# and remove old snapshots (including this one) at some point to save space.
# 
# 6.2  Exit from the `chroot` environment back to the LiveCD environment:
# 
#     # exit
# 
# 6.3  Run these commands in the LiveCD environment to unmount all filesystems:
# 
#     # mount | grep -v zfs | tac | awk '/\/mnt/ {print $3}' | xargs -i{} umount -lf {}
#     # zpool export "$ROOT_POOL"
# 
# 6.4  Reboot:
# 
#     # reboot
# 
# 6.5  Wait for the newly installed system to boot normally. Login as root.
# 
# 6.6  Create a user account:
# 
#     # zfs create "$ROOT_POOL"/home/YOURUSERNAME
#     # adduser YOURUSERNAME
#     # cp -a /etc/skel/.[!.]* /home/YOURUSERNAME
#     # chown -R YOURUSERNAME:YOURUSERNAME /home/YOURUSERNAME
# 
# 6.7  Add your user account to the default set of groups for an administrator:
# 
#     # usermod -a -G audio,cdrom,dip,floppy,netdev,plugdev,sudo,video YOURUSERNAME
# 
# 
# Step 7: Configure Swap
# ----------------------
# 
# 7.1  Create a volume dataset (zvol) for use as a swap device:
# 
#     # zfs create -V 4G -b $(getconf PAGESIZE) -o compression=zle \
#           -o logbias=throughput -o sync=always \
#           -o primarycache=metadata -o secondarycache=none \
#           -o com.sun:auto-snapshot=false "$ROOT_POOL"/swap
# 
# You can adjust the size (the `4G` part) to your needs.
# 
# The compression algorithm is set to `zle` because it is the cheapest
# available algorithm.  As this guide recommends `ashift=12` (4 kiB blocks on
# disk), the common case of a 4 kiB page size means that no compression
# algorithm can reduce I/O.  The exception is all-zero pages, which are dropped
# by ZFS; but some form of compression has to be enabled to get this behavior.
# 
# 7.2  Configure the swap device:
# 
# Choose one of the following options.  If you are going to do an encrypted
# home directory later, you should use encrypted swap.
# 
# 7.2a  Create an unencrypted (regular) swap device:
# 
# **Caution**: Always use long `/dev/zvol` aliases in configuration files.
# Never use a short `/dev/zdX` device name.
# 
#     # mkswap -f /dev/zvol/"$ROOT_POOL"/swap
#     # echo /dev/zvol/"$ROOT_POOL"/swap none swap defaults 0 0 >> /etc/fstab
# 
# 7.2b  Create an encrypted swap device:
# 
#     # echo cryptswap1 /dev/zvol/"$ROOT_POOL"/swap /dev/urandom \
#           swap,cipher=aes-xts-plain64:sha256,size=256 >> /etc/crypttab
#     # systemctl daemon-reload
#     # systemctl start systemd-cryptsetup@cryptswap1.service
#     # echo /dev/mapper/cryptswap1 none swap defaults 0 0 >> /etc/fstab
# 
# 7.3  Enable the swap device:
# 
#     # swapon -av
# 
# 
# Step 8: Full Software Installation
# ----------------------------------
# 
# 8.1  Upgrade the minimal system:
# 
#     # apt dist-upgrade --yes
# 
# 8.2  Optional: Disable log compression:
# 
# As `/var/log` is already compressed by ZFS, logrotate’s compression is going
# to burn CPU and disk I/O for (in most cases) very little gain.  Also, if you
# are making snapshots of `/var/log`, logrotate’s compression will actually
# waste space, as the uncompressed data will live on in the snapshot.  You can
# edit the files in `/etc/logrotate.d` by hand to comment out `compress`, or
# use this loop (copy-and-paste highly recommended):
# 
#     # for file in /etc/logrotate.d/* ; do
#         if grep -Eq "(^|[^#y])compress" "$file" ; then
#             sed -i -r "s/(^|[^#y])(compress)/\1#\2/" "$file"
#         fi
#     done
# 
# 8.3  Reboot:
# 
#     # reboot
# 
# 
# Step 9: Final Cleanup
# ---------------------
# 
# 9.1  Wait for the system to boot normally. Login using the account you
# created. Ensure the system (including networking) works normally.
# 
# 9.2  Optional: Delete the snapshot of the initial installation:
# 
#     $ sudo zfs destroy "$ROOT_POOL"/debian@install
# 
# 9.3  Optional: Disable the root password
# 
#     $ sudo usermod -p '*' root
# 
# 9.4  Optional (not recommended):
# 
# If you prefer the graphical boot process, you can re-enable it now. It will
# make debugging boot problems more difficult, though.
# 
#     $ sudo vi /etc/default/grub
#     Add quiet to GRUB_CMDLINE_LINUX_DEFAULT
#     Comment out GRUB_TERMINAL=console
#     Save and quit.
# 
#     $ sudo update-grub
# 
# Troubleshooting
# ---------------
# 
# ### Rescuing using a Live CD
# 
# Boot the Live CD and open a terminal.
# 
# Become root and install the ZFS utilities:
# 
#     $ sudo -i
#     # echo deb http://ftp.debian.org/debian jessie-backports main contrib \
#           >> /etc/apt/sources.list.d/backports.list
#     # apt update
#     # apt install --yes linux-headers-$(uname -r)
#     # apt install --yes -t jessie-backports zfs-dkms
# 
# This will automatically import your pool. Export it and re-import it to get
# the mounts right:
# 
#     # zpool export -a
#     # zpool import -N -R /mnt "$ROOT_POOL"
#     # zfs mount "$ROOT_POOL"/debian
#     # zfs mount -a
# 
# If needed, you can chroot into your installed environment:
# 
#     # mount --rbind /dev  /mnt/dev
#     # mount --rbind /proc /mnt/proc
#     # mount --rbind /sys  /mnt/sys
#     # chroot /mnt /bin/bash --login
# 
# Do whatever you need to do to fix your system.
# 
# When done, cleanup:
# 
#     # mount | grep -v zfs | tac | awk '/\/mnt/ {print $3}' | xargs -i{} umount -lf {}
#     # zpool export "$ROOT_POOL"
#     # reboot
# 

###
# Copyright notice
#
# This work is an adaptation by Sam Pablo Kuper of [Debian Jessie Root on
# ZFS](https://github.com/zfsonlinux/zfs/wiki/Debian Jessie Root on ZFS) by
# George Melikov and Richard Laager, which was itself an adaptation of [Ubuntu
# 16.04 Root on
# ZFS](https://github.com/zfsonlinux/zfs/wiki/Ubuntu-16.04-Root-on-ZFS) by
# Richard Laager.
# 
# - Copyright (C) 2016-2017 [Richard Laager](https://github.com/rlaager).
#   (Contributions
#   [licensed](https://github.com/zfsonlinux/zfs/issues/5960#issuecomment-291249575)
#   under [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/).) -
#
# - Copyright (C) 2017 [George Melikov](https://github.com/gmelikov).
#   (Contributions
#   [licensed](https://github.com/zfsonlinux/zfs/issues/5960#issuecomment-291416602)
#   under [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/).) -
#
# - Copyright (C) 2017 [Sam Pablo Kuper](https://gitlab.com/sampablokuper).
#   (Contributions offered under your choice of:
#     - [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/);
#     - [GPLv2](https://www.gnu.org/licenses/old-licenses/gpl-2.0.html) or any
#       later version of the GPL; or
#     - [CDDLv1.0](https://directory.fsf.org/wiki/License:CDDLv1.0).)
###
