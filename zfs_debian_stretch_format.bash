#!/usr/bin/env bash
###
# Copyright (C) 2017 [Sam Pablo Kuper](https://gitlab.com/sampablokuper).
# You may use this file under your choice of:
#  - [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/);
#  - [GPLv2](https://www.gnu.org/licenses/old-licenses/gpl-2.0.html) or any
#    later version of the GPL; or
#  - [CDDLv1.0](https://directory.fsf.org/wiki/License:CDDLv1.0).
###
# Intended usage
# ==============
# This script will format a drive (HDD, SSD, SSHD, or flash drive) as specified
# in the "Drive layout" section of
# Debian-Stretch-Boot-on-ZFS-Root-On-LUKS-Plus-ZFS.org
###

# Has set-up script been sourced? If not, source it.
[[ -v COMMON_SOURCED && "$COMMON_SOURCED" -eq 1 ]] \
	|| source zfs_debian_stretch_common.bash $1
wait

## Remove possible remnants of previous runs of this script
source zfs_debian_stretch_nuke.bash $1
wait


partprobe

## Partition target drive
# 1MB BIOS boot partition
sgdisk -a2048 -n1:2048:4095     -t1:EF02 $1 -c 1:"bios_boot_partition"
wait
# 510MB partition for /boot ZFS filesystem
sgdisk -a2048 -n2:4096:1052671  -t2:BF07 $1 -c 2:"zfs_boot_partition"
wait
# Remaining drive space, except the last 510MiB in case of future need:
# partition to hold the LUKS container and the root ZFS filesystem
sgdisk -a2048 \
-n"$CRYPT_PARTITION_NUM":1052672:-510M \
-t"$CRYPT_PARTITION_NUM":8300 $1 \
-c "$CRYPT_PARTITION_NUM":"luks_zfs_root_partition"
wait

# Before proceeding, ensure /dev/disk/by-id/ knows of these new partitions
partprobe
wait

# Create the /boot pool
zpool create -o ashift=12            \
             -O atime=off            \
             -O canmount=off         \
	     -O compression=lz4      \
	     -O normalization=formD  \
             -O mountpoint=/boot     \
             -R /mnt                 \
             $BOOT_POOL "$1"-part2
wait

# Create the LUKS container for the root pool
cryptsetup luksFormat "$1"-part3               \
                      --hash sha512            \
                      --cipher aes-xts-plain64 \
		      --key-size 512
wait

# Open LUKS container that will contain the root pool
cryptsetup luksOpen "$1"-part3 "$CRYPT_PARTITION_SHORTNAME"
wait

# Create the root pool
zpool create -o ashift=12           \
             -O atime=off           \
             -O canmount=off        \
             -O compression=lz4     \
             -O normalization=formD \
             -O mountpoint=/        \
             -R /mnt                \
             $ROOT_POOL /dev/mapper/"$CRYPT_PARTITION_SHORTNAME"
wait
