
# Zero from the start of the drive to the data area of the /boot zpool
# i.e. LBA 0 to LBA 12287 inclusive

# Zero from /boot vdev label 2 to the data area of the root zpool
# i.e. LBA 1051648 to LBA 1064903 inclusive

# Zero the last 512MiB of the drive, to zero root vdev labels 2 & 3 plus the
# secondary GPT


$(((6628*1024)/512))
