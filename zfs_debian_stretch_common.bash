#!/usr/bin/env bash
###
# Copyright (C) 2017 [Sam Pablo Kuper](https://gitlab.com/sampablokuper).
# You may use this file under your choice of:
#  - [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/);
#  - [GPLv2](https://www.gnu.org/licenses/old-licenses/gpl-2.0.html) or any
#    later version of the GPL; or
#  - [CDDLv1.0](https://directory.fsf.org/wiki/License:CDDLv1.0).)
###
# Intended usage
# ==============
# This script provides some common functionality for the scripts referenced by
# Debian-Stretch-Boot-on-ZFS-Root-On-LUKS-Plus-ZFS.org
###

set -x
set -e

# Validate input argument(s) 
die () {
    echo >&2 "$@"
    exit 1
}
[[ "$#" -eq 1 ]] || die "Exactly 1 argument required: path to disk ID. $# given"
[[ -b "$1" ]]    || die "$1 is not a block device"
[[ "$1" == /dev/disk/by-id/* ]] || die "$1 does not begin '/dev/disk/by-id/'"

# Activate the ZFS kernel module
/sbin/modprobe zfs

## Set variables

# Block device-related variables
DRIVE_SHORTNAME=$(ls -l $1 | sed 's,^.*/\(\w*\)$,\1,') # E.g. "sda".
CRYPT_PARTITION_NUM=3
CRYPT_PARTITION_SHORTNAME=\
"$DRIVE_SHORTNAME""$CRYPT_PARTITION_NUM"_crypt # E.g. "sda3_crypt".
CRYPT_PARTITION_UUID=$(blkid -o value -s UUID "$1"-part"$CRYPT_PARTITION_NUM")\
 || sleep 0

# Zpool-related variables
BOOT_POOL=bpool                    # Name for zpool containing '/boot'.
ROOT_POOL=rpool                    # Name for zpool containing root ('/').

# FHS-related variables
NON_MOUNT_POINT_DIRS=(bin etc lib lib64 opt sbin usr) # *NOT* mount points.
MOUNT_POINT_DIRS_TMP_TOP=(tmp)     # Dirs for tmp files, directly under '/'.
MOUNT_POINT_DIRS_TMP_SUB=(var/tmp) # Dirs for tmp files, under subdirs of '/'.
MOUNT_POINT_DIRS_NON_TMP_TOP=(boot home root srv var)  # Similarly, non-tmp.
MOUNT_POINT_DIRS_NON_TMP_SUB=(var/cache var/log var/mail var/spool) # & so on.
VIRTUAL_FILESYSTEM_DIRS=(dev proc sys)
FILES_TO_COPY=(initrd.img initrd.img.old vmlinuz vmlinuz.old)
DIRS_TO_COPY=("${MOUNT_POINT_DIRS_NON_TMP_TOP[@]}" "${NON_MOUNT_POINT_DIRS[@]}")

# Set flag to show this file has been sourced
COMMON_SOURCED=1
