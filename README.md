**Pull requests very welcome.**

**However:** I may occasionally squash commits in this repository until either:

- everything in it is working well, or
- I learn that someone else has earnestly based commits on top of mine,

whichever comes first.

So, if you are basing commits on mine and don't want to have to rebase, please file an issue and let me know!
