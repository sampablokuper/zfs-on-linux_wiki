#!/usr/bin/env bash
###
# Copyright (C) 2017 [Sam Pablo Kuper](https://gitlab.com/sampablokuper).
# You may use this file under your choice of:
#  - [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/);
#  - [GPLv2](https://www.gnu.org/licenses/old-licenses/gpl-2.0.html) or any
#    later version of the GPL; or
#  - [CDDLv1.0](https://directory.fsf.org/wiki/License:CDDLv1.0).)
###
# Intended usage
# ==============
# This script will configure GRUB2 and the /boot partition in order to make
# the Debian Stretch on ZFS with LUKS installation bootable, as described at
# Debian-Stretch-Boot-on-ZFS-Root-On-LUKS-Plus-ZFS.org
###

# Has set-up script been sourced? If not, source it.
[[ -v COMMON_SOURCED && "$COMMON_SOURCED" -eq 1 ]] \
	|| source zfs_debian_stretch_common.bash $1
wait

# Check that CRYPT_PARTITION_UUID has been set
[[ -v CRYPT_PARTITION_UUID && "$CRYPT_PARTITION_UUID" ==\
 $(blkid -o value -s UUID "$1"-part"$CRYPT_PARTITION_NUM") ]]
# Populate /etc/crypttab
echo "$CRYPT_PARTITION_SHORTNAME" UUID="$CRYPT_PARTITION_UUID" none luks,loud \
> /etc/crypttab

# TODO Populate /etc/fstab
# "$1"-part2 /boot zfs defaults 0 0
# /dev/mapper/"$CRYPT_PARTITION_SHORTNAME" / zfs defaults 0 0
# swap?
# CD?
mawk '\
$3~/swap/ {next} \
$3!~/ext[2-4]/ {gsub(/ +/, " "); print} \
$3~/ext[2-4]/ {$3="zfs"; print}' \
/etc/fstab

# TODO Append " zfs-mount" to "$local_fs" line of /etc/insserv.conf

# TODO Populate /etc/init.d/zfs-mount


# Configure GRUB2 to wait for user input...
# ... and to provide verbose output.
# (`sed` does not have non-greedy regular expressions, so we use
# two invocations here, where one would otherwise suffice, in order
# to account for possible whitespace one or both sides of 'quiet'.)
sed -i -E -e 's/^(GRUB_TIMEOUT=)[[:digit:]]+/\1-1/'                       \
       -e 's/^(GRUB_CMDLINE_LINUX_DEFAULT=".*)(quiet | quiet)(.*")/\1\3/' \
       -e 's/^(GRUB_CMDLINE_LINUX_DEFAULT=")quiet(")/\1\2/'               \
       /etc/default/grub

# Set up GRUB2 for legacy (MBR) booting. ***TODO: is this correct?***
grub-install "$1"
echo '`update-initramfs` should produce "Generating\
 /boot/initrd.img-4.9.0-2-amd64" or similar'
update-initramfs -u -k all
echo '`grub-probe /` should produce "zfs"'
grub-probe /
 
# You may be presented with a dialog box that says, "The grub-pc package is being
# upgraded. This menu allows you to select which devices you'd like grub-install
# to be automatically run for, if any." Choose `/dev/sda` (assuming you are
# installing to that drive) rather than one of the partitions on the drive (i.e.
# don't choose `/dev/sda3`). This should give you a message along the lines:
# 
#     Installing for i386-pc platform.
#     Installation finished. No error reported.
# 
# Step 5: GRUB Installation
# -------------------------
# 
# 5.3  Optional (but highly recommended): Make debugging GRUB easier:
# 
#     # vi /etc/default/grub
#     Uncomment: GRUB_TERMINAL=console
#     Save and quit.
# 
# Later, once the system has rebooted twice and you are sure everything is
# working, you can undo these changes, if desired.
# 
# 5.4  Update the boot configuration:
# 
#     # update-grub
#     Generating grub configuration file ...
#     Found linux image: /boot/vmlinuz-4.9.0-2-amd64
#     Found initrd image: /boot/initrd.img-4.9.0-2-amd64
#     done
# 
# 5.5  Install the boot loader
# 
# 5.5a  For legacy (MBR) booting, install GRUB to the MBR:
# 
#     # grub-install /dev/disk/by-id/scsi-SATA_disk1
#     Installing for i386-pc platform.
#     Installation finished. No error reported.
# 
# Do not reboot the computer until you get exactly that result message. Note
# that you are installing GRUB to the whole disk, not a partition.
# 
# If you are creating a mirror, repeat the grub-install command for each disk
# in the pool.
# 
# 5.6  Verify that the ZFS module is installed:
# 
#     # ls /boot/grub/*/zfs.mod
