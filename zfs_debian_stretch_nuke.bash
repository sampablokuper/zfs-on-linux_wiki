#!/usr/bin/env bash
###
# Copyright (C) 2017 [Sam Pablo Kuper](https://gitlab.com/sampablokuper).
# You may use this file under your choice of:
#  - [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/);
#  - [GPLv2](https://www.gnu.org/licenses/old-licenses/gpl-2.0.html) or any
#    later version of the GPL; or
#  - [CDDLv1.0](https://directory.fsf.org/wiki/License:CDDLv1.0).
###
# Intended usage
# ==============
# This script should not be run directly. It is intended to be sourced by
# zfs_debian_stretch_format.bash .
###

# Has set-up script been sourced? If not, source it.
[[ -v COMMON_SOURCED && "$COMMON_SOURCED" -eq 1 ]] \
	|| source zfs_debian_stretch_common.bash $1
wait

## Delete and unmount non-virtual filesystems from target
# Delete non-mount point files
for i in "${NON_MOUNT_POINT_DIRS[@]}" "${FILES_TO_COPY[@]}"; do
    rm -r /mnt/"$i" || sleep 0
done
# Delete contents of mount point files, and unmount them
for i in "${MOUNT_POINT_DIRS_TMP_SUB[@]}"\
	 "${MOUNT_POINT_DIRS_NON_TMP_SUB[@]}"\
	 "${MOUNT_POINT_DIRS_TMP_TOP[@]}"\
	 "${MOUNT_POINT_DIRS_NON_TMP_TOP[@]}"; do
    rm -r /mnt/"$i"/* || sleep 0
    umount /mnt/"$i"  || sleep 0
done

# Remove recursively bound virtual filesystems from target
for i in "${VIRTUAL_FILESYSTEM_DIRS[@]}"; do
    # First unmount them
    umount --recursive --verbose --force /mnt/"$i" || sleep 0
    wait
    # Then delete their mount points
    rmdir /mnt/"$i" || sleep 0
    wait
done

# exit 1

# Destroy relevant zpools, if present
zpool destroy $BOOT_POOL || sleep 0
zpool destroy $ROOT_POOL || sleep 0

# Close LUKS container for root zpool, if open
cryptsetup luksClose "$DRIVE_SHORTNAME"3_crypt || sleep 0

# Just in case the drive previously held a filesystem or part of a RAID
# array, etc: attempt to zero the MD superblock and any filesystem headers that
# might be present. This reduces exposure to some [obscure causes of data
# corruption](https://github.com/zfsonlinux/zfs/issues/3430).
mdadm --zero-superblock --force $1
wait
wipefs --all --force $1
wait

# Delete MBR and GPT data structures
sgdisk --zap-all
wait

# Zero from the start of the drive to the data area of the /boot zpool
# i.e. LBA 0 to LBA 12287 inclusive
FIRST=0
LAST=12287
dd if=/dev/zero of=$1 bs=512 seek=$FIRST count=$(($LAST-$FIRST))

# Zero from /boot vdev label 2 to the data area of the root zpool
# i.e. LBA 1051648 to LBA 1064903 inclusive
FIRST=1051648
LAST=1064903
dd if=/dev/zero of=$1 bs=512 seek=$FIRST count=$(($LAST-$FIRST))

# Zero the first and last 10MiB of the last 512MiB of the drive, to be confident
# of zeroing the root vdev labels 2 & 3 plus the secondary GPT
MIB10_IN_LBA_SECTORS=$(((10*1024*1024)/512))
MIB512_IN_LBA_SECTORS=$(((512*1024*1024)/512))
DRIVE_SIZE_IN_LOGICAL_SECTORS=$(cat /sys/block/$DRIVE_SHORTNAME/size)
DRIVE_LOGICAL_SECTOR_SIZE=$(cat /sys/block/$DRIVE_SHORTNAME/queue/logical_block_size)
LAST=$((($DRIVE_SIZE_IN_LOGICAL_SECTORS*$DRIVE_LOGICAL_SECTOR_SIZE)/512))
FIRST=$(($LAST-$MIB512_IN_LBA_SECTORS))
SECOND=$(($LAST-$MIB10_IN_LBA_SECTORS))
dd if=/dev/zero of=$1 bs=512 seek=$FIRST  count=$MIB10_IN_LBA_SECTORS
dd if=/dev/zero of=$1 bs=512 seek=$SECOND count=$MIB10_IN_LBA_SECTORS
wait
