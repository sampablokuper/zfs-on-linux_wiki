Preamble
--------

### Caution

- No warranty or liability is provided. Use these instructions at your own
  risk.

### Considerations

#### Master Boot Record (MBR) vs GUID Partition Table (GPT)

The [MBR](https://en.wikipedia.org/wiki/Master_Boot_Record) is an old-fashioned
mechanism for storing a drive's partition table on the drive itself. It was
published in 1983. Its primary advantages are that it is very simple; and even
very old versions of popular PC operating systems support it. Its primary
disadvantages are that it does not readily support drives larger than ~2TiB; it
supports a maximum of four primary partitions; and it exists in only one place
on each drive, so is vulnerable to corruption.

The [(GPT)](https://en.wikipedia.org/wiki/GUID_Partition_Table) is a newer
mechanism for storing a drive's partition table on the drive itself. Popular PC
operating systems started to support GPT from around 2005. It is more complex
than the MBR, but avoids all three disadvantages mentioned above.

We do not need more than four primary partitions, so in that respect we could
use either system. But we do want resilience against data loss, and we may well
want to use drives larger than 2TiB. So **we will use GPT, not MBR**.

#### Boot loader

There are numerous boot loaders in existence. The default boot loader for
Debian Stretch is [GNU GRUB, version
2](https://en.wikipedia.org/wiki/GNU_GRUB). It is popular, reliable, and
well-documented, and it supports ZFS. So, it is what we will use.

#### Sector size, block size, and partition alignment

HDDs, SSHDs, and SSDs are block storage devices. That is, instead of reading or
writing data bit-by-bit, they read and write data in "blocks" (sometimes also
called "pages") of many bits.

The block sizes of the target drives, and of drives we might want to replace
our target drives with in the future, affect a number of things, including:

- the best value to use for ZFS's `ashift` parameter,
- where best to place partition boundaries;
- the amount of drive space allocated to the various parts of the GPT.

For maximum performance, the filesystem should send data to and request data
from the drive in blocks the same size as those the drive uses to read, write,
and overwrite data internally. This is so that the drive's firmware does not
have to translate the incoming or outgoing data from the filesystem's block
size to the drive's block size or vice versa, nor access more blocks than
necessary.

Likewise, for maximum performance, partitions should be aligned to drive block
boundaries, and ideally - in the case of HDDs and SSHDs - also to cylinder
boundaries.

This is not straightforward, especially if we want to futureproof our system by
ensuring that upgrades to newer hard drives or SSDs will retain maximum
performance. The reason it is not straightforward is that HDDs, SSHDs, and
especially SSDs, vary in the block sizes they use internally.

On HDDs and SSHDs, these blocks correspond to [physical
sectors](https://en.wikipedia.org/wiki/Disk_sector). For decades, most PC HDDs
were built with 512-byte sectors. This sector size is known as "legacy
format", "512 native", or "512n". It is being phased out in favor of [Advanced
Format](https://en.wikipedia.org/wiki/Advanced_Format) technology, which uses
4096-byte sectors. There are two kinds of Advanced Format drive: "512e" (aka
"512 emulation") which use an internal translation layer to present 512-byte
logical sectors to the computer for the sake of compatibility with archaic
software, even though the drive uses 4096-byte physical sectors; and "4K
native" (aka "4Kn") drives, which have 4096-byte physical sectors and no
translation layer. All three kinds of HDD - 512n, 512e, and 4Kn
- are still quite common, as of early 2017.

SSDs are even more complicated. They will report their supposed logical sector
sizes and physical sector sizes to the operating system, but these values
should be treated sceptically. They are typically lies that the SSD firmware
has been programmed to tell, in order to ensure backwards compatibility with
software that was written with HDDs in mind and which therefore expects drives
to have physical sectors (and perhaps logical sectors). SSDs, however, do not
have physical sectors.

SSDs read and write in "pages". Due to limitations of the kind of microchips
used in most SSDs, SSDs [can only write to free (unused)
pages](https://en.wikipedia.org/wiki/File:Garbage_Collection.png). In other
words, SSDs cannot directly overwrite pages.

Also, SSDs cannot erase single pages, they can only erase pages in groups
called "erase blocks". Every page on an SSD belongs to exactly one erase block.
As SSDs cannot directly overwrite pages, they instead simulate overwriting with
a process called
[read-erase-modify-write](https://en.wikipedia.org/wiki/Write_amplification).

SSD page and erase block sizes are not always specified by the manufacturers.
However, models reportedly differ in their page sizes, erase block sizes, and
the number of pages they use per erase block. For example:

- [Samsung 840 250 GB SSD](https://forums.gentoo.org/viewtopic-t-963066-start-0-postdays-0-postorder-asc-highlight-.html): 
    - Page size: 8KiB
    - Erase block size: 1536KiB
    - 1536/8 = 192 pages per block

- [OCZ Vertex 3](https://superuser.com/a/492092):
    - Page size: 8KiB
    - Erase block size: 2MiB
    - 2048/8 = 256 pages per block

- [Sandisk Extreme II](http://forums.sandisk.com/t5/SanDisk-Extreme-II-SSD-Legacy/erase-block-size-for-240GB-Extreme-II/td-p/305631):
    - Page size: 8KiB
    - Erase block size: 4MiB
    - 4096/8 = 512 pages per block

- [Crucial M550](http://forums.crucial.com/t5/Crucial-SSDs/M550-Erase-Block-Size/td-p/155238)
    - Page size: 16KiB
    - Erase block size: 8MiB
    - 8192/16 = 512 pages per block

The historical trend seems to be towards small absolute increases in page size,
and large absolute increases in erase block size.

##### Partition alignment

###### HDDs and SSHDs

With HDDs and SSHDs, partitions should, ideally, begin and end at cylinder
boundaries, for best performance as this allows data at the start and end of
the partition to be read without the heads needing to skip tracks.

However, determining the cylinder boundaries is not easy. Modern HDDs and SSHDs
are programmed to
[misreport](https://ubuntuforums.org/showthread.php?t=1704055&s=b431d9c86215805694f4a339b986bee3&p=10545988#post10545988)
their [drive geometry](https://en.wikipedia.org/wiki/Cylinder-head-sector).
Originally, in the 1990s, the rationale for this misreporting was to maintain
[compatibility with legacy
BIOS](http://pcguide.com/ref/hdd/geom/geomLogical-c.html): the manufacturers
would still print the physical drive geometry on the drive label, or in the
specification sheet, enabling the user to determine the cylinder boundaries.
Nowadays, though, manufacturers rarely provide such detail on drive labels or
specification sheets. Maybe it saves costs by enabling a model's physical
geometry to be changed during a production run without having to update its
documentation. Maybe it protects the manufacturers' trade secrets.  Whatever
the reason, it means the only way to estimate the cylinder boundaries is to
reverse-engineer the drive.

An example of a modern drive whose manufacturer does not provide enough
information to determine the cylinder size is the Seagate FireCuda 2.5" 1TB
ST1000LX015. The drive's manual states:

- Guaranteed sectors: 1,953,525,168
- Heads: 2
- Disks: 1
- Bytes per sector: 512 (logical) / 4096 (physical)

The "guaranteed sectors" spec must refer to logical sectors, because

    1,953,525,168 x 512 = 1,000,204,886,016

which is almost exactly 1TB. So, we can deduce that the drive has

    1,953,525,168 * (512 / 4096) = 244,190,646

physical sectors. And we know that the drive has 2 heads, so we can also deduce
that the drive has

    244,190,646 / 2 = 122,095,323

physical sectors per head. From this, we can deduce that the drive probably has
a number of cylinders that 122,095,323 will divide into with no remainder, but
we still do not know *which* number that is. It could be 3 or 9 or 21 or 27,
etc.  In principle, we could infer it by reading and writing suitably-sized
pieces of data to and from the disk, with suitable offsets, and seeing which
sized pieces with which offsets give the highest bitrate, but we do not
currently know of a program that automates this reliably, and to do it manually
would be incredibly laborious.

Because of the difficulty of knowing where the cylinder boundaries lie with
modern HDDs, recent versions of partitioning tools such as `parted`, `GPT
fdisk`, and `fdisk` no longer try to optimise partition boundaries to cylinder
boundaries. Instead, by default, they align partitions to offsets from the
start of the disk that are multiples of 1MiB.

This means that although a partition might start in the middle of a track, it
will at least start at both a logical and physical sector boundary, regardless
of whether the drive uses 512-byte or 4096-byte logical or physical sectors.

###### SSDs

Partition alignment [also matters with
SSDs](https://www-ssl.intel.com/content/www/us/en/solid-state-drives/ssd-partition-alignment-tech-brief.html).
The author of `GPT fdisk` recommends aligning partitions with erase block
boundaries. However, as we noted above, SSDs vary in their page and erase block
sizes, and it is not always clear how large the erase blocks are.

The default 1MiB partition alignment increment used by `fdisk`, `parted`, and
`GPT fdisk` is clearly too small to reliably align with the erase blocks of
recent SSDs with erase block sizes of 2MiB, 4MiB, or 8MiB, let alone for future
SSDs, whose erase blocks may be larger still.

If we do not know the size of our SSD's erase blocks, which alignment value
should we use? Of those SSD models whose erase block sizes are known, all
appear to be powers of 2 except for the Samsung 840 Evo series, which used a
1.5MiB erase block. (The 840 Evo series were afflicted with serious performance
problems. The proprietary nature of the 840 Evo firmware means that we can only
speculate whether these problems related to the obvious mismatch between the
erase block size and the default 1MiB alignment used by most partitioning
tools.)

For this HOWTO, we will take our inspiration from Apple. Starting in 2006
with [OS X 10.6 Snow Leopard](http://www.rodsbooks.com/gdisk/advice.html), Apple left
128MiB of free space after each partition "[to make it easier for future system
software to manipulate the partition map in ways that we can't anticipate
currently.](https://developer.apple.com/library/content/technotes/tn2166/_index.html)"
This was probably not specifically intended for SSDs, as SSDs did not become
common until several years later. Instead of leaving 128MiB of free space at
the end of each partition, we suggest aligning partitions to 128MiB on SSDs.
This will ensure alignment with erase block boundaries for the foreseeable
future.

##### ashift

ZFS on Linux has a parameter called `ashift` that controls the size of the
filesystem blocks. `ashift` takes an integer value. Raising the number 2 to the
power of `ashift` gives the block size in octets. For example,
2<sup>9</sup>=512; 2<sup>12</sup>=4096; 2<sup>13</sup>=8192. As of early 2017,
`ashift` can only take integer values from 9 to 13.

Unfortunately, when a disk in a vdev is
[**replaced**](http://wiki.illumos.org/display/illumos/ZFS+and+Advanced+Format+disks),
the original `ashift` value will be applied. Likewise, when a disk is
[**attached to a
mirror**](http://wiki.illumos.org/display/illumos/ZFS+and+Advanced+Format+disks),
the `ashift` value used to create the mirror will be used.

[fixed and
cannot later be
increased](https://utcc.utoronto.ca/~cks/space/blog/solaris/ZFS4KSectorDisks).

If you later need to migrate data from a vdev to a new vdev that, due to `ashift` incompatibility, cannot reasonably be added to the existing vdev's zpool, then you will need to incorporate the new vdev into a new zpool and copy the data from the old zpool to the new zpool.

The Open ZFS on OS X wiki puts it concisely:

> The default for `ashift` is 9, but you will most likely want to create pools
> with an `ashift` of 12, which is appropriate for 4096 (4k) disks (i.e.,
> Advanced Format disks), even if your disks are still the older 512. If you
> are using SSDs in your pool now, or anticipate replacing any of your pool's
> disks with SSDs in the future, then an `ashift` of 13 is a better choice. It
> is important to get this right now because a vdev's `ashift` cannot be
> changed after the vdev is created.



This immutability is unfortunate, because if at some point in the future the
zpool's drives need to be replaced, and the zpool's ashift value is suboptimal
for the new drives, then the zpool will perform poorly.


Normally, for optimal performance, the ZFS pool's block size should match the
physical sector size (in the case of HDDs and SSHDs) or the page size (in the
case of SSDs). Obviously, due to the maximum `ashift` value corresponding to
8KiB block sizes, SSDs with page sizes larger than this will have a mismatch
between their page size and the ZFS block size.

Exceptionally, some SSDs have internal controllers that are optimized for
specific block sizes despite having larger page sizes (e.g. the Intel DC S3500
is optimized for 4KiB blocks despite having an 8KiB page size). When the ZFS on
Linux developers learn about such drives, they typically
[hard-code](http://list.zfsonlinux.org/pipermail/zfs-discuss/2014-June/016263.html)
ZFS on Linux to use the optimal `ashift` value for the drive.

[vary so widely in their
internal
behaviour](http://list.zfsonlinux.org/pipermail/zfs-discuss/2015-October/023512.html)


Finally, the smaller the files are that you wish to store, relative to the
block size utilised by ZFS, the
[worse](http://yas.ch/blog/2015/05/06/playing-around-with-zfs-ashift/) the
[internal
fragmentation](https://en.wikipedia.org/w/index.php?title=Fragmentation_%28computing%29&oldid=764696241#Internal_fragmentation)
efficiency.

We therefore adopt an approach that should be compatible with all three kinds
of drive, in order to ensure that as old drives wear out and are replaced, they
will be able to be replaced with modern 512e or 4Kn drives.  However, this
compatibility will come at a performance cost for 512n drives: 512e or 4Kn
drives will be more performant. 
```
|====================================================================================================================|
| Drive  | Logical | Physical |            ashift=9         |            ashift=12        |         ashift=13        |
| format | sector  | sector   |-----------------------------|-----------------------------|--------------------------|
|        | size    | size     | Performance    | Internal   | Performance    | Internal   | Performance | Internal   |
|        | (bytes) | (bytes)  |                | frag.      |                | frag.      |             | frag.      |
|        |         |          |                | efficiency |                | efficiency |             | efficiency |
|========|===========================================================================================================|
| 512n   |     512 |      512 | Optimal        | High       | Subobtimal     | Medium     | Subobtimal  | Low        |
|--------|---------|----------|----------------|------------|----------------|------------|-------------|------------|
| 512e   |     512 |     4096 | Subobtimal     | High       | Optimal        | Medium     | Subobtimal  | Low        |
|--------|---------|----------|----------------|------------|----------------|------------|-------------|------------|
| 4Kn    |    4096 |     4096 | (Incompatible) | (N/A)      | Optimal        | Medium     | Subobtimal  | Low        |
|--------|---------|----------|----------------|------------|----------------|------------|-------------|------------|
| 8Ke    |     512 |     8192 | Suboptimal     | High       | Subobtimal     | Medium     | Optimal     | Low        |
|--------|---------|----------|----------------|------------|----------------|------------|-------------|------------|
| 8Kn    |    8192 |     8192 | (Incompatible) | (N/A)      | (Incompatible) | Medium     | Optimal     | Low        |
|====================================================================================================================|
```
###### HDDs and SSHDs

##### Sector size


```
|==================================================================================================================|
| Partition  | Region    | Region purpose              | Start                   | End                 | Size      |
| name (on   | type      |                             |-------------------------|---------------------|           |
| 1st drive) |           |                             | LBA     | Bytes/&c      | LBA (or | Octets    |           |
|            |           |                             |         |               | sgdisk  |           |           |
|            |           |                             |         |               | offset) |           |           |
|==================================================================================================================|
| N/A        | MBR       | Protective MBR              |       0 |         0 B   |       0 |       511 |   512 B   |
|------------------------------------------------------------------------------------------------------------------|
| N/A        | Primary   | Primary GPT Header          |       1 |       512 B   |       1 |      1023 |   512 B   |
|            | GPT       |-----------------------------------------------------------------------------------------|
|            |           | GPT partition table         |       2 |         1 KiB |      33 |     17407 |    16 KiB |
|------------------------------------------------------------------------------------------------------------------|
| N/A        | N/A       | Blank space for alignment   |      34 |        17 KiB |    2047 |   1048575 |  1007 KiB |
|------------------------------------------------------------------------------------------------------------------|
| /dev/sda1  | EF02      | BIOS boot partition         |    2048 |         1 MiB |    4095 |   2097151 |     1 MiB |
|------------------------------------------------------------------------------------------------------------------|
| /dev/sda2  | BF07      | /boot partition using ZFS   |    4096 |         2 MiB | 1052671 | 538968064 |   512 MiB |
|            |           |-----------------------------------------------------------------------------------------|
|            |           | ZFS vdev label 0            |    4096 |         2 MiB |    4607 |   2359295 |   256 KiB |
|            |           |-----------------------------------------------------------------------------------------|
|            |           | ZFS vdev label 1            |    4608 |      2.25 MiB |    5119 |   2621439 |   256 KiB |
|            |           |-----------------------------------------------------------------------------------------|
|            |           | ZFS vdev boot block         |    5120 |       2.5 MiB |   12287 |   6291455 |   3.5 MiB |
|            |           |-----------------------------------------------------------------------------------------|
|            |           | ZFS zpool data (for /boot ) |   12288 |         6 MiB | 1051647 | 538443775 | 507.5 MiB |
|            |           |-----------------------------------------------------------------------------------------|
|            |           | ZFS vdev label 2            | 1051648 |     513.5 MiB | 1052159 | 538705919 |   256 KiB |
|            |           |-----------------------------------------------------------------------------------------|
|            |           | ZFS vdev label 3            | 1052160 |    513.75 MiB | 1052671 | 538968063 |   256 KiB |
|------------------------------------------------------------------------------------------------------------------|
| /dev/sda3  | 8300      | LUKS container              | 1052672 |       514 MiB | (-510M) |    End of |     Drive |
|            |           |                             |         |               |         |     drive |     minus |
|            |           |                             |         |               |         |     minus |     1 GiB |
|            |           |                             |         |               |         |   510 MiB |           |
|            |           |-----------------------------------------------------------------------------------------|
|            |           | LUKS phdr                   | 1052672 |       514 MiB | 1052673 | 538968655 |   592 B   |
|            |           |-----------------------------------------------------------------------------------------|
|            |           | LUKS: blank space for 4KiB  | 1052673 | 538968656 B   | 1052679 | 538972159 |  3504 B   |
|            |           | alignment within header     |         |               |         |           |           |
|            |           |-----------------------------------------------------------------------------------------|
|            |           | LUKS key material section 1 | 1052680 |    526340 KiB | 1053183 | 539230207 |   252 KiB |
|            |           |-----------------------------------------------------------------------------------------|
|            |           | LUKS key material section 2 | 1053184 |    526592 KiB | 1053687 | 539488255 |   252 KiB |
|            |           |-----------------------------------------------------------------------------------------|
|            |           | LUKS key material section 3 | 1053688 |    526844 KiB | 1054191 | 539746303 |   252 KiB |
|            |           |-----------------------------------------------------------------------------------------|
|            |           | LUKS key material section 4 | 1054192 |    527096 KiB | 1054695 | 540004351 |   252 KiB |
|            |           |-----------------------------------------------------------------------------------------|
|            |           | LUKS key material section 5 | 1054696 |    527348 KiB | 1055199 | 540262399 |   252 KiB |
|            |           |-----------------------------------------------------------------------------------------|
|            |           | LUKS key material section 6 | 1055200 |    527600 KiB | 1055703 | 540520447 |   252 KiB |
|            |           |-----------------------------------------------------------------------------------------|
|            |           | LUKS key material section 7 | 1055704 |    527852 KiB | 1056207 | 540778495 |   252 KiB |
|            |           |-----------------------------------------------------------------------------------------|
|            |           | LUKS key material section 8 | 1056208 |    528104 KiB | 1056711 | 541036543 |   252 KiB |
|            |           |-----------------------------------------------------------------------------------------|
|            |           | ZFS vdev label 0            | 1056712 |    528356 KiB | 1057223 | 541298687 |   256 KiB |
|            |           |-----------------------------------------------------------------------------------------|
|            |           | ZFS vdev label 1            | 1057224 |    528612 KiB | 1057735 | 541560831 |   256 KiB |
|            |           |-----------------------------------------------------------------------------------------|
|            |           | ZFS vdev boot block         | 1057736 |    528868 KiB | 1064903 | 545230847 |   3.5 MiB |
|            |           |-----------------------------------------------------------------------------------------|
|            |           | ZFS bulk data: root & swap  | 1064904 |    532452 KiB |     TBC |       TBC |   Size of |
|            |           |                             |         |               |         |           | /dev/sda3 |
|            |           |                             |         |               |         |           |     minus |
|            |           |                             |         |               |         |           |  6628 KiB |
|            |           |-----------------------------------------------------------------------------------------|
|            |           | ZFS vdev label 2            |     TBC |           TBC |     TBC |       TBC |   256 KiB |
|            |           |-----------------------------------------------------------------------------------------|
|            |           | ZFS vdev label 3            |     TBC |           TBC | (-510M) |       TBC |   256 KiB |
|------------------------------------------------------------------------------------------------------------------|
| N/A        | N/A       | Blank space in case of need |     TBC |           TBC |     TBC |       TBC |  ~510 MiB |
|------------------------------------------------------------------------------------------------------------------|
| N/A        | Secondary | Secondary GPT Header        |     TBC |           TBC |     TBC |       TBC |   512 B   |
|            | GPT       |-----------------------------------------------------------------------------------------|
|            |           | GPT partition table         |     TBC |           TBC |     TBC |       EOD |    16 KiB |
|==================================================================================================================|

```


**Notes:**
* The use of `ashift=12` is recommended here because many drives today have 4KiB (or larger) physical sectors, even though they present 512B logical sectors.  Also, a future replacement drive may have 4KiB physical sectors (in which case `ashift=12` is desirable) or 4KiB logical sectors (in which case `ashift=12` is required).
* Setting `normalization=formD` eliminates some corner cases relating to UTF-8 filename normalization. It also implies `utf8only=on`, which means that only UTF-8 filenames are allowed. If you care to support non-UTF-8 filenames, do not use this option. For a discussion of why requiring UTF-8 filenames may be a bad idea, see [The problems with enforced UTF-8 only filenames](http://utcc.utoronto.ca/~cks/space/blog/linux/ForcedUTF8Filenames).
* Make sure to include the `-part1` portion of the drive path. If you forget that, you are specifying the whole disk, which ZFS will then re-partition, and you will lose the bootloader partition(s).

**Hints:**
* The root pool does not have to be a single disk; it can have a mirror or raidz topology.  In that case, repeat the partitioning commands for all the disks which will be part of the pool. Then, create the pool using `zpool create ... rpool mirror /dev/disk/by-id/scsi-SATA_disk1-part1 /dev/disk/by-id/scsi-SATA_disk2-part1` (or replace `mirror` with `raidz`, `raidz2`, or `raidz3` and list the partitions from additional disks). Later, install GRUB to all the disks. This is trivial for MBR booting; the UEFI equivalent is currently left as an exercise for the reader.
* The pool name is arbitrary.  On systems that can automatically install to ZFS, the root pool is named `rpool` by default.  If you work with multiple systems, it might be wise to use `hostname`, `hostname0`, or `hostname-1` instead.




Copyright and license
---------------------

This work is an adaptation by Sam Pablo Kuper of [Debian Jessie Root on
ZFS](https://github.com/zfsonlinux/zfs/wiki/Debian Jessie Root on ZFS) by
George Melikov and Richard Laager. Sam Pablo Kuper's contributions
are published under the [Creative Commons Attribution ShareAlike license
3.0 (CC BY SA 3.0)](https://creativecommons.org/licenses/by sa/3.0/).
